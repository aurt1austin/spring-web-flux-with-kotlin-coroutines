package com.example.demo.controller

import com.example.demo.DemoApplication
import com.example.demo.model.Item
import com.example.demo.repository.ItemRepository
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.runApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders.ACCEPT
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBodyList

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ExtendWith(SpringExtension::class)
class ApplicationTest {

    protected val client = WebTestClient.bindToServer()
        .baseUrl("http://127.0.0.1:8080")
        .defaultHeader(ACCEPT, MediaType.APPLICATION_JSON_VALUE)
        .build()

    @BeforeAll
    fun initApplication() {
        runApplication<DemoApplication>()
    }
}

class TestController : ApplicationTest() {

    @Test
    fun getBooks() {
        client.get().uri("/items/findAll").exchange()
            .expectStatus().isOk
            .expectBodyList<Item>()
            .hasSize(2)
    }

}