package com.example.demo.controller

import com.example.demo.model.Item
import com.example.demo.repository.ItemRepository
import kotlinx.coroutines.flow.Flow
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/items")
class ItemController (private val itemRepository: ItemRepository){

    @GetMapping("/findByNameContains")
    suspend fun findByNameContains(@RequestParam("itemName") itemName: String): List<Item> {
        return itemRepository.findByNameContains(itemName);
    }

    @GetMapping("/findAll")
    suspend fun findAll(): Flow<Item> {
        return itemRepository.findAll();
    }

}