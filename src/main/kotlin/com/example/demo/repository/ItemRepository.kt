package com.example.demo.repository

import com.example.demo.model.Item
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface ItemRepository : CoroutineCrudRepository<Item, Int> {

    suspend fun findByNameContains(name: String): List<Item>;

}