package com.example.demo.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table


@Table
data class Item(@Id var id: Int = 0, var name: String = "", var age: Int = 0)